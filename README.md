
# ESP32 - Контрола на степ мотор со жироскоп (Controlling a Step Motor with a Gyroscope)

Тука ќе ги најдете сите потребни датотеки за да го изработите овој проект!

Here you will find all the necessary files to make this project!


## Слика (Picture)

![Preview](Preview.png)


## Содржина (Content)

 - **Libraries** папка - Тука ги има сите потребни библиотеки (Библиотеките не се направени од мене туку од https://github.com/jrowberg, jrowberg);
 - **Fritzing (for connecting the wires)** папка - Тука можете да најдете проект на Fritzing за шемата (кој најчесто нема да ви биде потребен), но и сликата за поврзување на компонентите која ќе ви биде потребна;
 -  **Arduino IDE Project** папка - Во оваа папка ќе го најдете Arduino проектот, со наставката .ino, кој ви е потребен за да го изработите проектот.

**English**
 
 - **Libraries** folder - Here are all the necessary libraries (The libraries are not made by me but by https://github.com/jrowberg, jrowberg);
 - **Fritzing (for connecting the wires)** folder - Here you can find a Fritzing project for the schematic (which most of the time you won't need), but also the image for connecting the components which you will need;
 - **Arduino IDE Project** folder - In this folder you will find the Arduino project, with the extension .ino, which is needed to make the project.


## Автор (Author)

- Автор на видеото и на сите дадотеки освен на библиотеките [@sapcpro_sergij](https://gitlab.com/sapcpro_sergij).
  
  Author of the video and all files except the libraries [@sapcpro_sergij](https://gitlab.com/sapcpro_sergij).


## Лиценца (License)

Лиценцата на овој проект е следната (The license of the folowing project is):
**[MIT](LICENSE)**

*Други лиценци (Other Licenses)*
 - [I2CDevLib (MIT License)](https://github.com/jrowberg/i2cdevlib/issues/209)

