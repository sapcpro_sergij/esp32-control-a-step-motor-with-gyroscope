// Code by Sergij Aleksovski (SApcPro)
// https://gitlab.com/sapcpro_sergij/esp32-control-a-step-motor-with-gyroscope
// MIT License
// https://gitlab.com/sapcpro_sergij/esp32-control-a-step-motor-with-gyroscope/-/blob/main/LICENSE
// 2023

#include <Wire.h>
#include <I2Cdev.h>
#include <MPU6050.h>
#include <Stepper.h>

// change this to fit the number of steps per revolution
const int stepsPerRevolution = 32;

// for your motor
MPU6050 mpu;

// ULN2003 Motor Driver Pins
#define IN1 19
#define IN2 18
#define IN3 5
#define IN4 17

int16_t ax, ay, az;
int16_t gx, gy, gz;

struct MyData {
  byte X;
  byte Y;
};

MyData data;

// initialize the stepper library on pins 8 through 11:
// Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);
Stepper myStepper(stepsPerRevolution, IN1, IN3, IN2, IN4);

int stepCount = 0;  // number of steps the motor has taken
void setup() {
  Serial.begin(9600);
  Wire.begin();
  mpu.initialize();
}

void loop() {
  mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  data.X = map(ax, -17000, 17000, 0, 255);  // X axis data
  data.Y = map(ay, -17000, 17000, 0, 255);  // Y axis data

  //delay(500);

  Serial.print(" Axis X = ");
  Serial.print(data.X);
  Serial.print("  ");
  Serial.print(" Axis Y = ");
  Serial.println(data.Y);

  if (data.Y < 110) {
    delay(1);
    myStepper.setSpeed(900);
    myStepper.step(300);
  }
  if (data.Y > 135) {
    myStepper.setSpeed(900);
    myStepper.step(-300);
    delay(1);
  }
}
